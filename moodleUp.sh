#!/bin/bash

MOODLEDIR="/home/onion/www/university-of-the-commons"
MOODLECURVER=`grep '$release' ${MOODLEDIR}/version.php | cut -d "'" -f 2`

printf "Moodle version to be upgraded: ${MOODLECURVER} \n\n"
read -p 'Live moodle directory(eg: /home/onion/www/university-of-the-commons): ' MOODLEDIR
read -p 'Moodle version (eg: 37 not 3.7): ' MOODLEVER

if [ ! -d ${HOME}/tmp ]; then
  # Control will enter here if $DIRECTORY doesn't exist.
  echo "Creating new  ~/tmp directory"
  mkdir ${HOME}/tmp
fi
# cd to working
pushd  ${HOME}/tmp

# fresh start for moodle file
sudo rm -rf moodle*

# Make Backups first
pushd  ${HOME}/tmp

NOW=date +"%m-%d-%y-%H-%M"
BAKUPDIR=mdle_${MOODLEVER}_update_bak-${NOW}
mkdir $BAKUPDIR
pushd $BAKUPDIR

printf $BAKUPDIR

echo "backing up code at${MOODLEDIR}"
tar czf moodlebak.tar.gz $MOODLEDIR --checkpoint=.800
printf "\n done backing up code :) \n"

echo "backing up postgres db"
#read config files
CONFIG=$MOODLEDIR/config.php

dbname=`grep dbname ${CONFIG} | cut -d "'" -f 2`
dbuser=`grep dbuser ${CONFIG} | cut -d "'" -f 2`
dbpass=`grep dbpass ${CONFIG} | cut -d "'" -f 2`
sudo -s -u postgres pg_dump ${dbname} -N topology -T spatial_ref_sys > dbexport.pgsql
tail dbexport.pgsql -n 5

popd

# get latest moodle - change in future
wget http://sourceforge.net/projects/moodle/files/Moodle/stable${MOODLEVER}/moodle-latest-${MOODLEVER}.tgz
tar xzf moodle-latest-*.tgz
rm moodle-latest-*.tgz

# copy stuff from old install to new
for f in .git .gitignore config.php theme/eguru local/staticpage
do
 echo "Copying $f"
 cp -R $MOODLEDIR/$f moodle/$f
done
printf "done"

read -p "Updated code now at: ${HOME}/tmp/moodle. Replace server code now? (y/n)" -n 1 -r
echo    # (optional) move to a new line
if [[ $REPLY =~ ^[Yy]$ ]]
then
    mv $MOODLEDIR $HOME/tmp/$BAKUPDIR
    mv moodle $MOODLEDIR

    pushd $MOODLEDIR
    pwd

    # database upgrades
    printf "updating database!\n"
    /usr/bin/php admin/cli/upgrade.php

    rm -r ../moodledata/cache/*


    read -p "Commit this? (y/n)" -n 1 -r
    if [[ $REPLY =~ ^[Yy]$ ]]
    then
      git add -A
      git commit -m "upgrading moodle to version ${MOODLEVER}"
    fi
    popd
fi

popd

printf "sooo done"
